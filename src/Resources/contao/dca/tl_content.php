<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_imagetext'] =
    '{type_legend},type;' .
    '{headline_legend},headline,dse_subheadline,dse_secondline;' .
    '{text_legend},text,dse_imgtxtbgcolor;' .
    '{price_legend},dse_priceheadline,dse_price;' .
    '{image_legend},addImage,dse_imageText;' .
    '{link_legend},url,target,dse_linkTitle,dse_titleText;' .
    '{custom_legend},dse_isFullwidth;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['floating']['options']      = ['left', 'right'];
$GLOBALS['TL_DCA']['tl_content']['fields']['url']['eval']['mandatory'] = false;

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w100'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_priceheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_priceheadline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_price'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_price'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_titleText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_titleText'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imgtxtbgcolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_imgtxtbgcolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        'bg-black',
        'bg-middlegrey',
        'bg-lightgrey',
        'bg-ultralightgrey',
        'bg-white'
    ],
    'reference' => [
        'none'              => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#FFF',
            '#000',
            'Keine'
        ),
        'bg-black'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#141312',
            '#FFF',
            'Black'
        ),
        'bg-middlegrey'     => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#8D8986',
            '#000',
            'MiddleGrey'
        ),
        'bg-lightgrey'      => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#C6C4C2',
            '#000',
            'LightGrey'
        ),
        'bg-ultralightgrey' => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#E3E2E2',
            '#000',
            'UltralightGrey'
        ),
        'bg-white'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#F7F7F7',
            '#000',
            'NaturalWhite'
        )
    ],
    'eval'      => [
        'tl_class' => 'clr'
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_imageText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_imageText'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isFullwidth'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
