<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_imagetext'] = ['Bild/Text', 'Bild links|rechts und Text rechts|links'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Schlagzeileneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_secondline']   =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline']  =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];
    
$GLOBALS['TL_LANG']['tl_content']['dse_imgtxtbgcolor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['price_legend']   = 'Preiseinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_priceheadline']  =
    ['Preis Titel', 'Hier können Sie einen Titel für das Preisfeld hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_price']  =
    ['Preis', 'Hier können Sie einen Preistext hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_imageText']  =
    ['Bildtext', 'Hier können Sie einen Text hinzufügen, der auf dem Bild hover angezeigt wird.'];

$GLOBALS['TL_LANG']['tl_content']['custom_legend']   = 'Benutzerdefinierte Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth']  =
    ['Gesamtbreite', 'Ist das ein Element voller Breite?'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];