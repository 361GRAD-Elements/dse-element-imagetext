<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_imagetext'] = ['Image/Text', 'Image left|right and Text right|left'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Headline settings';
$GLOBALS['TL_LANG']['tl_content']['dse_secondline']   =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline']  =
    ['Subheadline', 'Here you can add a Subheadline.'];
    
$GLOBALS['TL_LANG']['tl_content']['dse_imgtxtbgcolor']  =
    ['Background Color', 'Here you can add a Background Color.'];

$GLOBALS['TL_LANG']['tl_content']['price_legend']   = 'Price settings';
$GLOBALS['TL_LANG']['tl_content']['dse_priceheadline']  =
    ['Price title', 'Here you can add a Title for the price field.'];
$GLOBALS['TL_LANG']['tl_content']['dse_price']  =
    ['Price', 'Here you can add a Price Text.'];
$GLOBALS['TL_LANG']['tl_content']['dse_imageText']  =
    ['Image Text', 'Here you can add a Text that will be shown on image hover.'];

$GLOBALS['TL_LANG']['tl_content']['custom_legend']   = 'Custom settings';
$GLOBALS['TL_LANG']['tl_content']['dse_isFullwidth']  =
    ['Full width', 'Is this a fullwidth element?'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];